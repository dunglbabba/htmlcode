/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



function loadjqgrid(grid_selector, pager_selector, name_array, colmodel, grid_data, boolean, caption, height, check) {

    var lastSelection, index;
    console.log(boolean)
    var parent_column = $(grid_selector).closest('[class*="col-"]');
    //resize to fit page size
    $(window).on('resize.jqGrid', function () {
        $(grid_selector).jqGrid('setGridWidth', parent_column.width());
    })

    //resize on sidebar collapse/expand
    $(document).on('settings.ace.jqGrid', function (ev, event_name, collapsed) {
        if (event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed') {
            //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
            setTimeout(function () {
                $(grid_selector).jqGrid('setGridWidth', parent_column.width());
            }, 20);
        }
    })
    jQuery(grid_selector).jqGrid({
        //direction: "rtl",
        //for this example we are using local data

        data: grid_data,
        datatype: "local",

        height: height,
        colNames: name_array,
        colModel: colmodel,

        viewrecords: true,
        rowNum: 10,
        rowList: [10, 20, 30],
        pager: pager_selector,
        altRows: true,
        //toppager: true,
        loadComplete: function () {
            var table = this;
            setTimeout(function () {



                updatePagerIcons(table);


            }, 0);
        },

        ondblClickRow: editRow,
        multiselect: boolean,

        editurl: 'clientArray', //nothing is saved
        caption: caption,

        //,autowidth: true,


        /**
         ,
         grouping:true, 
         groupingView : { 
         groupField : ['name'],
         groupDataSorted : true,
         plusicon : 'fa fa-chevron-down bigger-110',
         minusicon : 'fa fa-chevron-up bigger-110'
         },
         caption: "Grouping"
         */

    });

    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size
    function editRow(id) {
        if (id && id !== lastSelection) {
            var grid = grid_selector;
            var ret = $(grid).jqGrid('getRowData', id);
            $(grid).jqGrid('restoreRow', lastSelection);
            var chiso;
            $(grid).jqGrid('editRow', id,
                    {keys: true,
                        errorfunc: function (response) {
                            alert("fails");
                        },
//                        successfunc: function (response)
//                        {
//                            for (var i = 0; i < colmodel.length; i++) {
//                                var property = colmodel[i].name;
//                                var getdata = JSON.parse(response.responseText)[property];
//                                if (i == 5) {
//                                    chiso = getdata;
//                                }
//                                $(grid_selector).jqGrid('setCell', ret.id, property, getdata);
//                            }
//                        },
//                        afterrestorefunc: function (reponse) {
//                            $(grid_selector).jqGrid('setCell', ret.id, colmodel[5].name, chiso);
//                        }
                    });
            lastSelection = id;
        }
    }



    //enable search/filter toolbar
    jQuery(grid_selector).jqGrid('filterToolbar', {defaultSearch: true, stringResult: true,
        defaultSearch: "cn", autosearch: true, searchoptions: {sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge', 'in', 'ni']}, });



    //switch element when editing inline
    function aceSwitch(cellvalue, options, cell) {
        setTimeout(function () {
            $(cell).find('input[type=checkbox]')
                    .addClass('ace ace-switch ace-switch-5')
                    .after('<span class="lbl"></span>');
        }, 0);
    }
    //enable datepicker
    function pickDate(cellvalue, options, cell) {
        setTimeout(function () {
            $(cell).find('input[type=text]')
                    .datepicker({format: 'yyyy-mm-dd', autoclose: true});
        }, 0);
    }

    //navButtons

    //navButtons
    jQuery(grid_selector).jqGrid('navGrid', pager_selector,
            {//navbar options
                edit: false,
                editicon: 'ace-icon fa fa-pencil blue',
                add: false,
                addicon: 'ace-icon fa fa-plus-circle purple',
                del: false,
                delicon: 'ace-icon fa fa-trash-o red',
                search: true,
                searchicon: 'ace-icon fa fa-search orange',
                refresh: true,
                refreshicon: 'ace-icon fa fa-refresh green',
                view: false,
                viewicon: 'ace-icon fa fa-search-plus grey',
            },
            {
                //edit record form
                //closeAfterEdit: true,
                //width: 700,
                recreateForm: true,
                beforeShowForm: function (e) {
                    var form = $(e[0]);
                    form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar').wrapInner('<div class="widget-header" />')
                    style_edit_form(form);
                }
            },
            {
                //new record form
                //width: 700,
                closeAfterAdd: true,
                recreateForm: true,
                viewPagerButtons: false,
                beforeShowForm: function (e) {
                    var form = $(e[0]);
                    form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar')
                            .wrapInner('<div class="widget-header" />')
                    style_edit_form(form);
                }
            },
            {
                //delete record form
                recreateForm: true,
                beforeShowForm: function (e) {
                    var form = $(e[0]);
                    if (form.data('styled'))
                        return false;

                    form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar').wrapInner('<div class="widget-header" />')
                    style_delete_form(form);

                    form.data('styled', true);
                },
                onClick: function (e) {
                    //alert(1);
                }
            },
            {
                //search form
                recreateForm: true,
                afterShowSearch: function (e) {
                    var form = $(e[0]);
                    form.closest('.ui-jqdialog').find('.ui-jqdialog-title').wrap('<div class="widget-header" />')
                    style_search_form(form);
                },
                afterRedraw: function () {
                    style_search_filters($(this));
                }
                ,
                multipleSearch: true,
                /**
                 multipleGroup:true,
                 showQuery: true
                 */
            },
            {
                //view record form
                recreateForm: true,
                beforeShowForm: function (e) {
                    var form = $(e[0]);
                    form.closest('.ui-jqdialog').find('.ui-jqdialog-title').wrap('<div class="widget-header" />')
                }
            }
    )




    function style_edit_form(form) {
        //enable datepicker on "sdate" field and switches for "stock" field
        form.find('input[name=sdate]').datepicker({format: 'yyyy-mm-dd', autoclose: true})

        form.find('input[name=stock]').addClass('ace ace-switch ace-switch-5').after('<span class="lbl"></span>');
        //don't wrap inside a label element, the checkbox value won't be submitted (POST'ed)
        //.addClass('ace ace-switch ace-switch-5').wrap('<label class="inline" />').after('<span class="lbl"></span>');


        //update buttons classes
        var buttons = form.next().find('.EditButton .fm-button');
        buttons.addClass('btn btn-sm').find('[class*="-icon"]').hide();//ui-icon, s-icon
        buttons.eq(0).addClass('btn-primary').prepend('<i class="ace-icon fa fa-check"></i>');
        buttons.eq(1).prepend('<i class="ace-icon fa fa-times"></i>')

        buttons = form.next().find('.navButton a');
        buttons.find('.ui-icon').hide();
        buttons.eq(0).append('<i class="ace-icon fa fa-chevron-left"></i>');
        buttons.eq(1).append('<i class="ace-icon fa fa-chevron-right"></i>');
    }

    function style_delete_form(form) {
        var buttons = form.next().find('.EditButton .fm-button');
        buttons.addClass('btn btn-sm btn-white btn-round').find('[class*="-icon"]').hide();//ui-icon, s-icon
        buttons.eq(0).addClass('btn-danger').prepend('<i class="ace-icon fa fa-trash-o"></i>');
        buttons.eq(1).addClass('btn-default').prepend('<i class="ace-icon fa fa-times"></i>')
    }

    function style_search_filters(form) {
        form.find('.delete-rule').val('X');
        form.find('.add-rule').addClass('btn btn-xs btn-primary');
        form.find('.add-group').addClass('btn btn-xs btn-success');
        form.find('.delete-group').addClass('btn btn-xs btn-danger');
    }
    function style_search_form(form) {
        var dialog = form.closest('.ui-jqdialog');
        var buttons = dialog.find('.EditTable')
        buttons.find('.EditButton a[id*="_reset"]').addClass('btn btn-sm btn-info').find('.ui-icon').attr('class', 'ace-icon fa fa-retweet');
        buttons.find('.EditButton a[id*="_query"]').addClass('btn btn-sm btn-inverse').find('.ui-icon').attr('class', 'ace-icon fa fa-comment-o');
        buttons.find('.EditButton a[id*="_search"]').addClass('btn btn-sm btn-purple').find('.ui-icon').attr('class', 'ace-icon fa fa-search');
    }

    function beforeDeleteCallback(e) {
        var form = $(e[0]);
        if (form.data('styled'))
            return false;

        form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar').wrapInner('<div class="widget-header" />')
        style_delete_form(form);

        form.data('styled', true);
    }

    function beforeEditCallback(e) {
        var form = $(e[0]);
        form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar').wrapInner('<div class="widget-header" />')
        style_edit_form(form);
    }



    //it causes some flicker when reloading or navigating grid
    //it may be possible to have some custom formatter to do this as the grid is being created to prevent this
    //or go back to default browser checkbox styles for the grid
    function styleCheckbox(table) {
        /**
         $(table).find('input:checkbox').addClass('ace')
         .wrap('<label />')
         .after('<span class="lbl align-top" />')
         
         
         $('.ui-jqgrid-labels th[id*="_cb"]:first-child')
         .find('input.cbox[type=checkbox]').addClass('ace')
         .wrap('<label />').after('<span class="lbl align-top" />');
         */
    }


    //unlike navButtons icons, action icons in rows seem to be hard-coded
    //you can change them like this in here if you want
    function updateActionIcons(table) {
        /**
         var replacement = 
         {
         'ui-ace-icon fa fa-pencil' : 'ace-icon fa fa-pencil blue',
         'ui-ace-icon fa fa-trash-o' : 'ace-icon fa fa-trash-o red',
         'ui-icon-disk' : 'ace-icon fa fa-check green',
         'ui-icon-cancel' : 'ace-icon fa fa-times red'
         };
         $(table).find('.ui-pg-div span.ui-icon').each(function(){
         var icon = $(this);
         var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
         if($class in replacement) icon.attr('class', 'ui-icon '+replacement[$class]);
         })
         */
    }

    //replace icons with FontAwesome icons like above
    function updatePagerIcons(table) {
        var replacement =
                {
                    'ui-icon-seek-first': 'ace-icon fa fa-angle-double-left bigger-140',
                    'ui-icon-seek-prev': 'ace-icon fa fa-angle-left bigger-140',
                    'ui-icon-seek-next': 'ace-icon fa fa-angle-right bigger-140',
                    'ui-icon-seek-end': 'ace-icon fa fa-angle-double-right bigger-140'
                };
        $('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function () {
            var icon = $(this);
            var $class = $.trim(icon.attr('class').replace('ui-icon', ''));

            if ($class in replacement)
                icon.attr('class', 'ui-icon ' + replacement[$class]);
        })
    }

    function enableTooltips(table) {
        $('.navtable .ui-pg-button').tooltip({container: 'body'});
        $(table).find('.ui-pg-div').tooltip({container: 'body'});
    }

    //var selr = jQuery(grid_selector).jqGrid('getGridParam','selrow');

    $(document).one('ajaxloadstart.page', function (e) {
        $.jgrid.gridDestroy(grid_selector);
        $('.ui-jqdialog').remove();
    });

}


//subjqgrid
//
function subjqgrid(grid_selector, pager_selector, name_array, colmodel, grid_data, sub_grid, boolean, caption, height, check) {

    var lastSelection, index;
    console.log(boolean)
    var parent_column = $(grid_selector).closest('[class*="col-"]');
    //resize to fit page size
    $(window).on('resize.jqGrid', function () {
        $(grid_selector).jqGrid('setGridWidth', parent_column.width());
    })

    //resize on sidebar collapse/expand
    $(document).on('settings.ace.jqGrid', function (ev, event_name, collapsed) {
        if (event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed') {
            //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
            setTimeout(function () {
                $(grid_selector).jqGrid('setGridWidth', parent_column.width());
            }, 20);
        }
    })
    jQuery(grid_selector).jqGrid({
        //direction: "rtl",
        //for this example we are using local data
        subGrid: true,
        //subGridModel: [{ name : ['No','Item Name','Qty'], width : [55,200,80] }],
        //datatype: "xml",
        subGridOptions: {
            plusicon: "ace-icon fa fa-plus center bigger-110 blue",
            minusicon: "ace-icon fa fa-minus center bigger-110 blue",
            openicon: "ace-icon fa fa-chevron-right center orange"
        },
        //for this example we are using local data
        subGridRowExpanded: function (subgridDivId, rowId) {
            
            var subgridTableId = subgridDivId + "_t";
            console.log(rowId);
            console.log(subgridDivId);
            $("#" + subgridDivId).html("<table id='" + subgridTableId + "'></table>");
            var filter = sub_grid.filter(x => x.rowId === rowId);
            console.log(typeof filter);
            $("#" + subgridTableId).jqGrid({
                datatype: 'local',
                data: filter,
                //colNames: ['No','Item Name','Qty'],
                colModel: [
                    {name: 'id', width: 50},
                    {name: 'name', width: 150},
                    {name: 'qty', width: 50}
                ]
            });
        },
        data: grid_data,
        datatype: "local",

        height: height,
        colNames: name_array,
        colModel: colmodel,

        viewrecords: true,
        rowNum: 10,
        rowList: [10, 20, 30],
        pager: pager_selector,
        altRows: true,
        //toppager: true,
        loadComplete: function () {
            var table = this;
            setTimeout(function () {



                updatePagerIcons(table);


            }, 0);
        },

        multiselect: boolean,

        editurl: 'clientArray', //nothing is saved
        caption: caption,

        //,autowidth: true,


        /**
         ,
         grouping:true, 
         groupingView : { 
         groupField : ['name'],
         groupDataSorted : true,
         plusicon : 'fa fa-chevron-down bigger-110',
         minusicon : 'fa fa-chevron-up bigger-110'
         },
         caption: "Grouping"
         */

    });

    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size



    //enable search/filter toolbar
    jQuery(grid_selector).jqGrid('filterToolbar', {defaultSearch: true, stringResult: true,
        defaultSearch: "cn", autosearch: true, searchoptions: {sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge', 'in', 'ni']}, });



    //switch element when editing inline
    function aceSwitch(cellvalue, options, cell) {
        setTimeout(function () {
            $(cell).find('input[type=checkbox]')
                    .addClass('ace ace-switch ace-switch-5')
                    .after('<span class="lbl"></span>');
        }, 0);
    }
    //enable datepicker
    function pickDate(cellvalue, options, cell) {
        setTimeout(function () {
            $(cell).find('input[type=text]')
                    .datepicker({format: 'yyyy-mm-dd', autoclose: true});
        }, 0);
    }

    //navButtons

    //navButtons
    jQuery(grid_selector).jqGrid('navGrid', pager_selector,
            {//navbar options
                edit: false,
                editicon: 'ace-icon fa fa-pencil blue',
                add: false,
                addicon: 'ace-icon fa fa-plus-circle purple',
                del: false,
                delicon: 'ace-icon fa fa-trash-o red',
                search: true,
                searchicon: 'ace-icon fa fa-search orange',
                refresh: true,
                refreshicon: 'ace-icon fa fa-refresh green',
                view: false,
                viewicon: 'ace-icon fa fa-search-plus grey',
            },
            {
                //edit record form
                //closeAfterEdit: true,
                //width: 700,
                recreateForm: true,
                beforeShowForm: function (e) {
                    var form = $(e[0]);
                    form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar').wrapInner('<div class="widget-header" />')
                    style_edit_form(form);
                }
            },
            {
                //new record form
                //width: 700,
                closeAfterAdd: true,
                recreateForm: true,
                viewPagerButtons: false,
                beforeShowForm: function (e) {
                    var form = $(e[0]);
                    form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar')
                            .wrapInner('<div class="widget-header" />')
                    style_edit_form(form);
                }
            },
            {
                //delete record form
                recreateForm: true,
                beforeShowForm: function (e) {
                    var form = $(e[0]);
                    if (form.data('styled'))
                        return false;

                    form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar').wrapInner('<div class="widget-header" />')
                    style_delete_form(form);

                    form.data('styled', true);
                },
                onClick: function (e) {
                    //alert(1);
                }
            },
            {
                //search form
                recreateForm: true,
                afterShowSearch: function (e) {
                    var form = $(e[0]);
                    form.closest('.ui-jqdialog').find('.ui-jqdialog-title').wrap('<div class="widget-header" />')
                    style_search_form(form);
                },
                afterRedraw: function () {
                    style_search_filters($(this));
                }
                ,
                multipleSearch: true,
                /**
                 multipleGroup:true,
                 showQuery: true
                 */
            },
            {
                //view record form
                recreateForm: true,
                beforeShowForm: function (e) {
                    var form = $(e[0]);
                    form.closest('.ui-jqdialog').find('.ui-jqdialog-title').wrap('<div class="widget-header" />')
                }
            }
    )




    function style_edit_form(form) {
        //enable datepicker on "sdate" field and switches for "stock" field
        form.find('input[name=sdate]').datepicker({format: 'yyyy-mm-dd', autoclose: true})

        form.find('input[name=stock]').addClass('ace ace-switch ace-switch-5').after('<span class="lbl"></span>');
        //don't wrap inside a label element, the checkbox value won't be submitted (POST'ed)
        //.addClass('ace ace-switch ace-switch-5').wrap('<label class="inline" />').after('<span class="lbl"></span>');


        //update buttons classes
        var buttons = form.next().find('.EditButton .fm-button');
        buttons.addClass('btn btn-sm').find('[class*="-icon"]').hide();//ui-icon, s-icon
        buttons.eq(0).addClass('btn-primary').prepend('<i class="ace-icon fa fa-check"></i>');
        buttons.eq(1).prepend('<i class="ace-icon fa fa-times"></i>')

        buttons = form.next().find('.navButton a');
        buttons.find('.ui-icon').hide();
        buttons.eq(0).append('<i class="ace-icon fa fa-chevron-left"></i>');
        buttons.eq(1).append('<i class="ace-icon fa fa-chevron-right"></i>');
    }

    function style_delete_form(form) {
        var buttons = form.next().find('.EditButton .fm-button');
        buttons.addClass('btn btn-sm btn-white btn-round').find('[class*="-icon"]').hide();//ui-icon, s-icon
        buttons.eq(0).addClass('btn-danger').prepend('<i class="ace-icon fa fa-trash-o"></i>');
        buttons.eq(1).addClass('btn-default').prepend('<i class="ace-icon fa fa-times"></i>')
    }

    function style_search_filters(form) {
        form.find('.delete-rule').val('X');
        form.find('.add-rule').addClass('btn btn-xs btn-primary');
        form.find('.add-group').addClass('btn btn-xs btn-success');
        form.find('.delete-group').addClass('btn btn-xs btn-danger');
    }
    function style_search_form(form) {
        var dialog = form.closest('.ui-jqdialog');
        var buttons = dialog.find('.EditTable')
        buttons.find('.EditButton a[id*="_reset"]').addClass('btn btn-sm btn-info').find('.ui-icon').attr('class', 'ace-icon fa fa-retweet');
        buttons.find('.EditButton a[id*="_query"]').addClass('btn btn-sm btn-inverse').find('.ui-icon').attr('class', 'ace-icon fa fa-comment-o');
        buttons.find('.EditButton a[id*="_search"]').addClass('btn btn-sm btn-purple').find('.ui-icon').attr('class', 'ace-icon fa fa-search');
    }

    function beforeDeleteCallback(e) {
        var form = $(e[0]);
        if (form.data('styled'))
            return false;

        form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar').wrapInner('<div class="widget-header" />')
        style_delete_form(form);

        form.data('styled', true);
    }

    function beforeEditCallback(e) {
        var form = $(e[0]);
        form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar').wrapInner('<div class="widget-header" />')
        style_edit_form(form);
    }



    //it causes some flicker when reloading or navigating grid
    //it may be possible to have some custom formatter to do this as the grid is being created to prevent this
    //or go back to default browser checkbox styles for the grid
    function styleCheckbox(table) {
        /**
         $(table).find('input:checkbox').addClass('ace')
         .wrap('<label />')
         .after('<span class="lbl align-top" />')
         
         
         $('.ui-jqgrid-labels th[id*="_cb"]:first-child')
         .find('input.cbox[type=checkbox]').addClass('ace')
         .wrap('<label />').after('<span class="lbl align-top" />');
         */
    }


    //unlike navButtons icons, action icons in rows seem to be hard-coded
    //you can change them like this in here if you want
    function updateActionIcons(table) {
        /**
         var replacement = 
         {
         'ui-ace-icon fa fa-pencil' : 'ace-icon fa fa-pencil blue',
         'ui-ace-icon fa fa-trash-o' : 'ace-icon fa fa-trash-o red',
         'ui-icon-disk' : 'ace-icon fa fa-check green',
         'ui-icon-cancel' : 'ace-icon fa fa-times red'
         };
         $(table).find('.ui-pg-div span.ui-icon').each(function(){
         var icon = $(this);
         var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
         if($class in replacement) icon.attr('class', 'ui-icon '+replacement[$class]);
         })
         */
    }

    //replace icons with FontAwesome icons like above
    function updatePagerIcons(table) {
        var replacement =
                {
                    'ui-icon-seek-first': 'ace-icon fa fa-angle-double-left bigger-140',
                    'ui-icon-seek-prev': 'ace-icon fa fa-angle-left bigger-140',
                    'ui-icon-seek-next': 'ace-icon fa fa-angle-right bigger-140',
                    'ui-icon-seek-end': 'ace-icon fa fa-angle-double-right bigger-140'
                };
        $('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function () {
            var icon = $(this);
            var $class = $.trim(icon.attr('class').replace('ui-icon', ''));

            if ($class in replacement)
                icon.attr('class', 'ui-icon ' + replacement[$class]);
        })
    }

    function enableTooltips(table) {
        $('.navtable .ui-pg-button').tooltip({container: 'body'});
        $(table).find('.ui-pg-div').tooltip({container: 'body'});
    }

    //var selr = jQuery(grid_selector).jqGrid('getGridParam','selrow');

    $(document).one('ajaxloadstart.page', function (e) {
        $.jgrid.gridDestroy(grid_selector);
        $('.ui-jqdialog').remove();
    });

}
//=======================================================jqgrid for report============================================================
function loadjqgrid_report(grid_selector, pager_selector, name_array, colmodel, grid_data, boolean, caption, height, check) {

    var lastSelection, index;
    console.log(boolean)
    var parent_column = $(grid_selector).closest('[class*="col-"]');
    //resize to fit page size
    $(window).on('resize.jqGrid', function () {
        $(grid_selector).jqGrid('setGridWidth', parent_column.width());
    })

    //resize on sidebar collapse/expand
    $(document).on('settings.ace.jqGrid', function (ev, event_name, collapsed) {
        if (event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed') {
            //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
            setTimeout(function () {
                $(grid_selector).jqGrid('setGridWidth', parent_column.width());
            }, 20);
        }
    })
    jQuery(grid_selector).jqGrid({
        //direction: "rtl",
        //for this example we are using local data

        data: grid_data,
        datatype: "local",

        height: height,
        colNames: name_array,
        colModel: colmodel,

        viewrecords: true,
        rowNum: 10,

        pager: pager_selector,
        altRows: true,
        //toppager: true,
        loadComplete: function () {
            var table = this;
            setTimeout(function () {
                updatePagerIcons(table);

            }, 0);
        },

        caption: caption,

        //,autowidth: true,

        grouping: true,
        groupingView: {
            groupField: ['tuyen', 'address'],

            plusicon: 'fa fa-chevron-down bigger-110',
            minusicon: 'fa fa-chevron-up bigger-110',
            groupColumnShow: [false, false],
            groupText: [
                "Tuyến: <b>{0}</b>",
                "Đường: <b>{0}</b>",
            ],
            groupOrder: ["asc", "asc"],
            groupSummary: [true, true],
            groupSummaryPos: ['header', 'header'],
            groupCollapse: true,
            groupDataSorted: true


        },
        caption: "Danh sách khách hàng",
        footerrow: true,
        loadComplete: function () {
            var table = this;
            setTimeout(function () {
                updatePagerIcons(table);

            }, 0);
            var colSum = $(grid_selector).jqGrid('getCol', 'tongtien', false, 'sum');
            var colSumm3 = $(grid_selector).jqGrid('getCol', 'm3', false, 'sum');
            var colSummcount = $(grid_selector).jqGrid('getGridParam', 'records');
            ;
            $(grid_selector).jqGrid('footerData', 'set', {idcustomer: colSummcount, m3: colSumm3, tongtien: colSum});

        },

    });
    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size

    function updatePagerIcons(table) {
        var replacement =
                {
                    'ui-icon-seek-first': 'ace-icon fa fa-angle-double-left bigger-140',
                    'ui-icon-seek-prev': 'ace-icon fa fa-angle-left bigger-140',
                    'ui-icon-seek-next': 'ace-icon fa fa-angle-right bigger-140',
                    'ui-icon-seek-end': 'ace-icon fa fa-angle-double-right bigger-140'
                };
        $('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function () {
            var icon = $(this);
            var $class = $.trim(icon.attr('class').replace('ui-icon', ''));

            if ($class in replacement)
                icon.attr('class', 'ui-icon ' + replacement[$class]);
        })
    }
}
//====================================================Search report ===============================================================
function Searchreport(grid_selector) {
    $(grid_selector).jqGrid('clearGridData').jqGrid('setGridParam', {datatype: "local", data: grid_data_1}).trigger('reloadGrid');
}

//====================================================Trang hóa đơn================================================================
function viewinvoice(gridtable, name) {
    var i, selRowIds = $(gridtable).jqGrid("getGridParam", "selarrrow"), n, rowData;
    var listcustomer = '';
    for (i = 0, n = selRowIds.length; i < n; i++) {
        rowData = $(gridtable).jqGrid("getLocalRow", selRowIds[i]);
        console.log(rowData.id);
        // one can uses the data here
        listcustomer += rowData.name + '; ';

    }
    name.empty();
    name.text(listcustomer);


    loadjqgridmodal("#grid-table-modal", "#grid-pager-modal", list_array, list_colmodel, grid_data_3, true, 1100);

}
function confirminvoice(gridtable) {
    var i, selRowIds = $(gridtable).jqGrid("getGridParam", "selarrrow"), n, rowData;
    for (i = 0, n = selRowIds.length; i < n; i++) {
        rowData = $(gridtable).jqGrid("getLocalRow", selRowIds[i]);
        console.log(rowData.id);
        // one can uses the data here
    }
    BootstrapDialog.show({
        title: 'Say-hello dialog',
        message: 'Hi Apple!'
    });
    $(gridtable).jqGrid('clearGridData').jqGrid('setGridParam', {datatype: "local", data: grid_data_4}).trigger('reloadGrid');

}

function searchInvoice(month, street, loaihinh) {
    var streetlist = [];
    var loaihinhlist = [];
    console.log(street);
    console.log(loaihinh);
    streetlist = $(street).val();
    loaihinhlist = $(loaihinh).val();
//    


    //$("#grid-table").jqGrid('clearGridData').jqGrid('setGridParam', {datatype: "local", data: grid_data_1}).trigger('reloadGrid');
    // LoadtableforExcel('grid-table');
    $("#myModal").modal('show');
}


//=====================================================================Search enable==================================================================


function SetEnabledata(gridtable) {
    var i, selRowIds = $(gridtable).jqGrid("getGridParam", "selarrrow"), n, rowData;
    for (i = 0, n = selRowIds.length; i < n; i++) {
        rowData = $(gridtable).jqGrid("getLocalRow", selRowIds[i]);
        console.log(rowData.id);
        // one can uses the data here
    }
}
//=== === === === === === === === === === === === === === === === === === === === == Export excel === === === === === === === === === === === === === === === === === === === === === === === === =
function LoadtableforExcel(table) {

    var data = $('#' + table).jqGrid('getGridParam', 'data');
    ;
    var content = '<tr>';
    var header = $('#' + table).jqGrid('getGridParam', 'colNames');
    for (var i = 0; i < header.length; i++) {
        if (i != 0) {
            content += '<th>' + header[i] + '</th>'
        }
    }
    content += '</tr>'
    for (var i = 0; i < data.length; i++) {

        content += '<tr>';
        content += '<td>' + data[i]["id"] + '</td>';
        content += '<td>' + data[i].name + '</td>';
        content += '<td>' + data[i].address + '</td>';
        content += '<td>' + data[i].phone + '</td>';
        content += '<td>' + data[i].number1 + '</td>';
        content += '<td>' + data[i].number2 + '</td>';
        content += '<td>' + data[i].m3 + '</td>';
        content += '<td>' + data[i].money + '</td>';
        content += '<td>' + data[i].thueVAT + '</td>';
        content += '<td>' + data[i].thue + '</td>';
        content += '<td>' + data[i].tongtien + '</td>';
        content += '<td>' + data[i].nhanvien + '</td>';
        content += '</tr>';
    }
    $('#export tbody').html(content);
    return 'export';

}



function loadjqgridmodal(grid_selector, pager_selector, name_array, colmodel, grid_data, boolean, width) {
    jQuery(grid_selector).jqGrid({
        //direction: "rtl",
        //for this example we are using local data

        data: grid_data,
        datatype: "local",
        width: width,
        height: 500,
        colNames: name_array,
        colModel: colmodel,

        viewrecords: true,
        rowNum: 10,
        rowList: [10, 20, 30],
        pager: pager_selector,
        altRows: true,
        //toppager: true,
        multiselect: boolean,

        editurl: "./dummy.php", //nothing is saved
        caption: "Danh sách khách hàng thu tiền nước",
        onSelectRow: function (id) {
            if (id) {
                var arr = jQuery(grid_selector).jqGrid('getRowData', id);

                load_chiso(arr);
            }
        }

        //,autowidth: true,


        /**
         ,
         grouping:true, 
         groupingView : { 
         groupField : ['name'],
         groupDataSorted : true,
         plusicon : 'fa fa-chevron-down bigger-110',
         minusicon : 'fa fa-chevron-up bigger-110'
         },
         caption: "Grouping"
         */

    });
}
//===================================================================tiếp nhận vật tư==============================================================
function addvattu(jqgrid, vattu, soluong, arrayvattu) {
    var arrayidvattu = $(vattu).select2('data');
    var soluong = $(soluong).val();
    if (arrayidvattu.length == 0) {
        alert("Mời bạn lựa chọn vật tư");
    } else if (soluong != '') {
        for (var i = 0; i < arrayidvattu.length; i++) {
            var index = arrayvattu.findIndex((item) => item.id === arrayidvattu[i].id);
            if (index < 0) {
                var myrow = {id: arrayidvattu[i].id, tenvattu: arrayidvattu[i].text, number: soluong};
                arrayvattu.push(myrow);
            } else {
                arrayvattu[index].number = parseInt(arrayvattu[index].number) + parseInt(soluong);
            }
            $(jqgrid).jqGrid('clearGridData').jqGrid('setGridParam', {datatype: "local", data: arrayvattu}).trigger('reloadGrid');
        }
    } else if (alert("Bạn chưa nhập số lượng"))
        ;
}
function xoavattu(jqgrid) {
    var deleterow = $(jqgrid).jqGrid("getGridParam", "selarrrow");
    console.log(deleterow);
    var arrayvattu = [];
    for (var i = deleterow.length - 1; i >= 0; i--) {
        $(jqgrid).jqGrid('delRowData', deleterow[i]);
    }
    var data = $(jqgrid).jqGrid('getGridParam', 'data');
    ;
    return data;

}
//=====================================================================Xuất kho================================================================
function setmadongho(jqgrid, madongho) {
    var getfirstrow = $(jqgrid).jqGrid("getGridParam", "selarrrow");
    console.log(getfirstrow);
    var dongho = $(madongho).select2('data');
    console.log(dongho);
    $(jqgrid).jqGrid('setCell', getfirstrow[0], 'mavattu', dongho[0].text);
}

function xuatkho(jqgrid) {
    var getrow = $(jqgrid).jqGrid("getGridParam", "selarrrow");
    var arrayloaivattu = [];

    for (var i = 0; i < getrow.length; i++) {

        var celValue = $(jqgrid).getRowData(getrow[i]);

        var index = arrayloaivattu.findIndex((item) => item.id === celValue.maloaivattu);
        if (index < 0) {
            if (parseInt(celValue.soluong) > parseInt(celValue.tonkho)) {
                alert("Không thể xuất vật tư " + celValue.loaivattu + " số lượng tồn chỉ còn " + celValue.tonkho);
                console.log(celValue);

            } else {
                var myrow = {id: celValue.maloaivattu, name: celValue.loaivattu, soluong: celValue.soluong, madongho: celValue.mavattu, tonkho: celValue.tonkho};
                arrayloaivattu.push(myrow);
                console.log(arrayloaivattu);
            }
        } else {
            var sum = parseInt(arrayloaivattu[index].soluong) + parseInt(celValue.soluong);

            if (sum > parseInt(arrayloaivattu[index].tonkho)) {
                alert("Không thể xuất vật tư " + celValue.loaivattu + " số lượng tồn chỉ còn " + celValue.tonkho);
                console.log(celValue);
            } else {
                arrayloaivattu[index].soluong = parseInt(arrayloaivattu[index].soluong) + parseInt(celValue.soluong);
                console.log(arrayloaivattu);
            }
        }

    }
}

