!function(a) {
    "use strict";
    "function"==typeof define&&define.amd?define(["jquery", "../grid.base"], a): a(jQuery)
}

(function(a) {
    a.jgrid=a.jgrid|| {}
    , a.jgrid.hasOwnProperty("regional")||(a.jgrid.regional=[]), a.jgrid.regional.en= {
        defaults: {
            recordtext:"Xem {0} - {1} của {2}", emptyrecords:"Không có dòng nào để xem", loadtext:"Đang loading...", savetext:"Saving...", pgtext:"Trang {0} của {1}", pgfirst: "Trang đầu", pglast: "Trang cuối", pgnext: "Trang tiếp", pgprev: "Trang trước", pgrecs: "Records per Page", showhide: "Toggle Expand Collapse Grid", pagerCaption: "Grid::Page Settings", pageText: "Page:", recordPage: "Records per Page", nomorerecs: "No more records...", scrollPullup: "Pull up to load more...", scrollPulldown: "Pull down to refresh...", scrollRefresh: "Release to refresh..."
        }
        , search: {
            caption:"Tìm kiếm...", Find:"Tìm kiếm", Reset:"Trở lại", odata:[ {
                oper: "eq", text: "Bằng"
            }
           , {
                oper: "ne", text: "Không bằng"
            }
            , {
                oper: "lt", text: "nhỏ hơn"
            }
            , {
                oper: "le", text: "nhỏ hơn hoặc bằng"
            }
            , {
                oper: "gt", text: "Lớn hơn"
            }
            , {
                oper: "ge", text: "lớn hơn hoặc bằng"
            }
            , {
                oper: "bw", text: "bắt đầu với"
            }
            , {
                oper: "bn", text: "Không bắt đầu với"
            }
            , {
                oper: "in", text:  "trong"
            }
            , {
                oper: "ni", text: "ngoài"
            }
            , {
                oper: "ew", text: "kết thúc với"
            }
            , {
                oper: "en", text: "không kết thúc với"
            }
            , {
                oper: "cn", text: "chứa"
            }
            , {
                oper: "nc", text: "không chứa"
            }
            , {
                oper: "nu", text: "không có giá trị"
            }
            , {
                oper: "nn", text: "vô giá trị"
            }
            ], groupOps:[ {
                op: "AND", text: "và"
            }
            , {
                op: "OR", text: "hoặc"
            }
            ], operandTitle:"Click to select search operation.", resetTitle:"Reset Search Value"
        }
        , edit: {
            addCaption:"Add Record", editCaption:"Edit Record", bSubmit:"Submit", bCancel:"Cancel", bClose:"Close", saveData:"Data has been changed! Save changes?", bYes:"Yes", bNo:"No", bExit:"Cancel", msg: {
                required: "Field is required", number: "Please, enter valid number", minValue: "value must be greater than or equal to ", maxValue: "value must be less than or equal to", email: "is not a valid e-mail", integer: "Please, enter valid integer value", date: "Please, enter valid date value", url: "is not a valid URL. Prefix required ('http://' or 'https://')", nodefined: " is not defined!", novalue: " return value is required!", customarray: "Custom function should return array!", customfcheck: "Custom function should be present in case of custom checking!"
            }
        }
        , view: {
            caption: "View Record", bClose: "Close"
        }
        , del: {
            caption: "Delete", msg: "Delete selected record(s)?", bSubmit: "Delete", bCancel: "Cancel"
        }
        , nav: {
            edittext: "", edittitle: "Edit selected row", addtext: "", addtitle: "Add new row", deltext: "", deltitle: "Delete selected row", searchtext: "", searchtitle: "Find records", refreshtext: "", refreshtitle: "Reload Grid", alertcap: "Warning", alerttext: "Please, select row", viewtext: "", viewtitle: "View selected row", savetext: "", savetitle: "Save row", canceltext: "", canceltitle: "Cancel row editing", selectcaption: "Actions..."
        }
        , col: {
            caption: "Select columns", bSubmit: "Ok", bCancel: "Cancel"
        }
        , errors: {
            errcap: "Error", nourl: "No url is set", norecords: "No records to process", model: "Length of colNames <> colModel!"
        }
        , formatter: {
            integer: {
                thousandsSeparator: ",", defaultValue: "0"
            }
            , number: {
                decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, defaultValue: "0.00"
            }
            , currency: {
                decimalSeparator: ".", thousandsSeparator: ",", decimalPlaces: 2, prefix: "", suffix: "", defaultValue: "0.00"
            }
            , date: {
                dayNames:["Sun", "Mon", "Tue", "Wed", "Thr", "Fri", "Sat", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"], monthNames:["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], AmPm:["am", "pm", "AM", "PM"], S:function(a) {
                    return 11>a||a>13?["st", "nd", "rd", "th"][Math.min((a-1)%10, 3)]: "th"
                }
                , srcformat:"Y-m-d", newformat:"n/j/Y", parseRe:/[#%\\\/:_;., \t\s-]/, masks: {
                    ISO8601Long: "Y-m-d H:i:s", ISO8601Short: "Y-m-d", ShortDate: "n/j/Y", LongDate: "l, F d, Y", FullDateTime: "l, F d, Y g:i:s A", MonthDay: "F d", ShortTime: "g:i A", LongTime: "g:i:s A", SortableDateTime: "Y-m-d\\TH:i:s", UniversalSortableDateTime: "Y-m-d H:i:sO", YearMonth: "F, Y"
                }
                , reformatAfterEdit:!1, userLocalTime:!1
            }
            , baseLinkUrl:"", showAction:"", target:"", checkbox: {
                disabled: !0
            }
            , idName:"id"
        }
    }
}

);