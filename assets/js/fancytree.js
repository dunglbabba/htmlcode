/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function loadfancytree(treeData_1, treeData_2, tree1, tree2) {
    $(tree1).fancytree({
        checkbox: true,
        folder: false,
        icon: false,
        selectMode: 3,
        extensions: ["filter"],
        quicksearch: true,
        filter: {
            autoApply: true,
            mode: "hide"
        },
        source: treeData_1,

    });
    $(tree2).fancytree({
        checkbox: true,
        folder: false,
        icon: false,
        selectMode: 3,
        extensions: ["filter"],
        quicksearch: true,
        filter: {
            autoApply: true,
            mode: "hide"
        },
        source: treeData_2,

    });
}

function load_filetr(d, tree2) {
    $(d).keyup(function (e) {
        var n,
                opts = {
                    autoExpand: true,
                    leavesOnly: true
                },
                match = $(this).val();

        if ($("#regex").is(":checked")) {
            n = tree2.filterNodes(function (node) {
                return new RegExp(match, "i").test(node.title);
            }, opts);
        } else {
            n = tree2.filterNodes(match, opts);
        }

    }).focus();
}