/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 */
//======================================Nhập chỉ số nước===========================================
function cascadedropdownlist(parentId, childId) {
    parentId.on("change", function (e) {

        var file = '';
        if ($(this).val() == '1') {
            file += 'json01.json';
        } else {
            file += 'json02.json';
        }

        var newOptions = '<option value="">-- Lựa chọn --</option>';
        $.getJSON(file, function (items) {
            $(items.rows).each(function (index, value) {
                newOptions += '<option value="' + value.id + '">' + value.name + '</option>';
            });
            childId.select2('destroy').html(newOptions).prop('disable', true).select2();
            setbackground('month');
        });

    });
}
function setbackground(id){
    console.log(id);
    var r= '#select2-'+id+'-results';
    console.log(r);
    
    $("#select2-month-results").css('background-color','yellow');
}
function loaddata(childId, address, number) {
    childId.on("change", function (e) {


        address.empty();
        number.empty();
        address.text("10B Lê Hồng Phong");
        number.text("100");

    });


}
function savedata(childID) {
    var month = $("#month").val();
    console.log(month);
    var tuyen = $("#tuyen").val();
    console.log(tuyen);
    var street = $("#street").val();
    console.log(street);
    var name = $("#name").val();
    console.log(name);
    $("#street").val("4").trigger("change");
}

function editdata(childID) {
    var month = $("#month").val();
    console.log(month);
    var tuyen = $("#tuyen").val();
    console.log(tuyen);
    var street = $("#street").val();
    console.log(street);
    var name = $("#name").val();
    console.log(name);
    $("#street").val("4").trigger("change");
}

//==============================================================Khách hàng =============================================

function load_customer(arr) {
    if (arr != null) {
        $("#form-field-name").val(arr.name);
        $("#form-field-address").val(arr.address);
        $("#form-field-phone").val(arr.phone);
        $("#dichvu").val(arr.type).trigger("change");
        $("#form-field-mst").val(arr.mst);

    }
}
function add_customer() {

    $("#form-field-name").val();
    $("#form-field-address").val();
    $("#form-field-phone").val();
    $("#dichvu").val();
    console.log($("#dichvu").val());
    $("#form-field-mst").val();


}
function edit_customer() {

    $("#form-field-name").val();
    $("#form-field-address").val();
    $("#form-field-phone").val();
    $("#dichvu").val();
    console.log($("#dichvu").val());
    $("#form-field-mst").val();


}
//==============================================================dichvu=================================================
function load_dichvu(arr) {
    if (arr != null) {
        $("#form-field-name").val(arr.name);
        $("#form-field-tentat").val(arr.tentat);

        $("#tuyen").val(arr.type).trigger("change");
        $("#form-field-mst").val(arr.mst);
        $("#form-field-cost").val(arr.gia);
        $("form-field-vat").val(arr.thueVAT);
    }
}
function add_dichvu() {

    $("#form-field-name").val();
    $("#form-field-tentat").val();

    $("#tuyen").val(arr.type).trigger("change");
    $("#form-field-mst").val();
    $("#form-field-cost").val();
    $("form-field-vat").val();


}
function edit_dichvu() {

    $("#form-field-name").val();
    $("#form-field-tentat").val();
    console.log(arr.type);
    $("#tuyen").val(arr.type).trigger("change");
    $("#form-field-mst").val();
    $("#form-field-cost").val();
    $("form-field-vat").val();


}
//====================================================================mức phí==================================================
function load_mucphi(arr) {
    if (arr != null) {
        $("#form-field-name").val(arr.name);
        $("#type").val(arr.type).trigger("change");
        $("#form-field-percent").val(arr.percent);

    }
}

function add_mucphi() {
    $("#form-field-name").val();
    $("#type").val();
    $("#form-field-percent").val();

}
function edit_mucphi(arr) {
    $("#form-field-name").val();
    $("#type").val();
    $("#form-field-percent").val();
}
//====================================================================nhân viên==================================================
function load_member(arr) {
    if (arr != null) {
        $("#form-field-name").val(arr.name);
        $("#type").val(arr.type).trigger("change");
        $("#form-field-phone").val(arr.phone);
        $("#username").empty();
        $("#username").text("abbabunda79");

    }
}
function add_member() {
    $("#form-field-name").val();
    $("#type").val();
    $("#form-field-phone").val();

}
function edit_member() {
    $("#form-field-name").val();
    $("#type").val();
    $("#form-field-phone").val();

}
//=====================================================================nhóm tuyến đuòng==========================================================
function load_nhom_tuyen(arr) {
    $("#form-field-name").val();

}

function add_nhom_tuyen() {
    $("#form-field-name").val();

}

function edit_nhom_tuyen() {
    $("#form-field-name").val();
}
//====================================================================tuyến============================================================
function load_tuyen(arr) {
    $("#form-field-name").val(arr.name);
    $("#type").val(arr.group).trigger("change");
}



function add_tuyen() {
    $("#form-field-name").val();
    $("#type").val();
}

function edit_tuyen() {
    $("#form-field-name").val();
    $("#type").val();
}
//=====================================================================Giá chỉ số====================================================
function loadchiso() {

    loadjqgridmodal("#grid-table-modal", "#grid-pager-modal", name_array_modal, colmodel_modal, grid_data_modal, false, 1100);
}

function load_chiso(arr) {

    $("#form-field-chiso").val(arr.name);
    $("#cost").val(arr.cost);
}
function add_chiso() {
    $("#form-field-chiso").val();
    $("#cost").val();
    console.log($("#cost").val());
}
function edit_chiso() {
    $("#form-field-chiso").val();
    $("#cost").val();
}

//===================================================================Fancytree============================================
function fancytree(tree, sourcedata) {
    tree.fancytree({
        checkbox: true,
        selectMode: 3,
        folder: false,
        icon: false,
        extensions: ["filter"],
        quicksearch: true,
        filter: {
            autoApply: true,
            mode: "hide"
        },
        source: sourcedata,
        select: function (event, data) {

        }

    }
    );
}



function load_filetr(d, tree2) {
    $(d).keyup(function (e) {
        var n, opts = {
            autoExpand: true,
            leavesOnly: true
        }, match = $(this).val();

        if ($("#regex").is(":checked")) {
            n = tree2.filterNodes(function (node) {
                return new RegExp(match, "i").test(node.title);
            }, opts);
        } else {
            n = tree2.filterNodes(match, opts);
        }

    }).focus();
}

function viewdata(grid, datagrid, treeData, type) {
    var selected = $('' + grid).fancytree('getTree').getSelectedNodes();
    console.log(selected);
    var title;
    var key;
    $('' + datagrid).fancytree("getTree").visit(function (node) {
        node.setSelected(false);
    });
    if (selected.length == 0) {
        alert("Bạn chưa chọn thông tin muốn xem")
    } else {
        for (z in selected) {
            for (i in treeData) {
                if (type == '1') {
                    title = treeData[i].title;

                    key = treeData[i].key;

                } else if (type == '2') {
                    title = treeData[i].key;
                    key = treeData[i].title;
                }

                $('' + datagrid).fancytree("getTree").visit(function (node) {
                    if (selected[z].key == title) {
                        console.log(node.key);
                        if (node.key == key)
                            node.setSelected(true);

                    }
                });
            }
        }
        ;
    }
}

//====================================================================Thanh toán tiền nước===================================
function loadorder(childId, address, grid, total) {
   
    childId.on("change", function (e) {
        grid.jqGrid('clearGridData').jqGrid('setGridParam', {datatype: "local", data: grid_data_5}).trigger('reloadGrid');
        address.empty();
        address.text("10B Lê Hồng Phong");
         var sum = 0;
        for (var i = 0;i< grid.getDataIDs().length;i++) {
            var data=grid.jqGrid('getRowData', grid.getDataIDs()[i]);

            sum = sum+ parseInt(data.money,10);
            // one can uses the data here
        }
        total.empty();
        total.text(''+sum);
    });


}

function confirm(childID) {
    var tuyen = $("#tuyen").val();
    console.log(tuyen);
    var street = $("#street").val();
    console.log(street);
    var name = $("#name").val();
    console.log(name);
    $("#street").val("4").trigger("change");
}

function deleteconfirm(childID) {
    var month = $("#month").val();
    console.log(month);
    var tuyen = $("#tuyen").val();
    console.log(tuyen);
    var street = $("#street").val();
    console.log(street);
    var name = $("#name").val();
    console.log(name);
    $("#street").val("4").trigger("change");
}

//===================================================================Load data theo trang====================================================
function selectdatarow(jqgrid, type) {
    $("" + jqgrid).jqGrid('setGridParam', {ondblClickRow:
                function (id) {
                    if (id) {
                        var ret = $("" + jqgrid).jqGrid('getRowData', id);
                        switch (type) {
                            case "1":
                                load_customer(ret);
                                break;
                            case "2":
                                load_dichvu(ret);
                                break;
                            case "3":
                                load_mucphi(ret);
                                break;
                            case "4":
                                load_member(ret);
                                break;
                            case "5":
                                load_nhom_tuyen(ret);
                                break;
                            case "6":
                                load_tuyen(ret);
                                break;

                        }

                    }
                }});
}